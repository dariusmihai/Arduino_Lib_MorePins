MorePins - The Arduino library that gives you as many pins as you need
=====
This library provides an easy plug-and-play 
way of managing the pins on multiple Input or Output Shift Registers (see "Required Hardware" below for more information)

License: GPL v3    
https://www.gnu.org/licenses/gpl-3.0.txt    

**Examples can be found in the _examples_ directory in the library**    
 
 
**(!)Most examples should have a Fritzing diagram** in their directory.    
For example, lbraries/MorePins/examples/Handle_One_Button/Sketch_one_button.fzz

```                                                                   +----------------------------------------+
+---------------------------------+
|        A R D U I N O            |
+--+-+-+--------------------------+
   | | |
   | | |  
   | | |  +-----------------------+
   | | |--+                       |
   | +----+       IC A            +----+
   +------+   (See Required HW)   |    |
          +                       |    |
          +--+----+------------+--+    |
       IC    |    |            |       |
       Pins  +    +            +       |
             0    1   [...]    7       |
                                       |
          +-----------------------+    |
          |                       |    |
     +----+       IC B            +----+
     |    |   (See Required HW)   |
     |    |                       |
     |    +--+----+------------+--+
     | IC    |    |            |
     | Pins  +    +            +
     |       8    9   [...]    15
     | 
     |    +-----------------------+
     |    |                       |
     +----+       IC C [...]      |
          |   (See Required HW)   |
          |                       |
          +--+----+------------+--+
       IC    |    |            |
       Pins  +    +            +
             16   17   [...]   23

              +           +
              |  [ ... ]  |
              |           |
              v           v
 
```

Required Hardware
-----
- For More Inputs:
    - SN54HC165 or SN74HC165: 8 bit Parallel Load Shift Registers


- For More Outpus:
    - SN54HC595 or SN74HC595: 8 bit Parallel Load Shift Registers

Components
-----
- MoreInputs
    - Usage: #inlude <MoreInputs.h>


- MoreOutputs
    - Usage: N/A - Under development

Installation
-----

You can install this library in 3 different ways:
(!) For console options *$>* denotes your console prompt, it is not part of the command

**(!) IMPORTANT**
For **Manual(Git)** and **Arduino IDE** modes, please also install the following library:    
**ArduinoSTL** by Mike Matera    
It can be installed via Arduino IDE Sketch->Include Library -> Manage Libraries
Alternatively, it is available on GitHub at https://github.com/maniacbug/StandardCplusplus


1. Manual (Git)
   - Get it from https://gitlab.com/dariusmihai/Arduino_Lib_MorePins.git
   - Place the directory in your global libraries location.
   - Use it by #include <MoreInputs.h> in your sketch



2. Via Arduino IDE
   - Open Arduino IDE
   - Go to Sketch -> Include Library -> Manage Libraries
   - Search for MorePins using the search box.
   - Click on the Install button.
   - Done, You can now use it by #include <MoreInputs.h>. in your sketch.



3. Via PlatformIO IDE 
   - Go to Home -> Libraries -> Registry
   - Search for MorePins
   - Install it.



4. Via PlatformIO platformio.ini file (option 1)
   - Create your project as usual.
   - Open your platformio.ini file.
   - Add this at the beginning:
    ```
      [common_env_data]
      lib_deps_external =
          MorePins
    ```
   - Then add this in your [env] block:
    ```
    lib_deps = ${common_env_data.lib_deps_external}
    ```
   - Open a console and navigate in it to your project's root dir
   - Run the following command
    ```
    $> platformio lib update
    ```

5. Via PlatformIO platformio.ini file (option 2)
    - Add this to your platformio.ini file.
    ```
    [env:my_build_env]
    framework = arduino
    
    lib_deps =
      # Using a library name
      MorePins
    ```


6. Via PlatformIO command line
   - Open a terminal and go to you project's root dir.
   - Run the following command in that console:
    ```
    $> platformio lib install MorePins
    ```
   - Update it via running:
    ```
    $> platformio lib update
    ```

Usage for MoreInputs
---
**Examples can be found in the _examples_ directory in the library**    

**(!)Most examples should have a Fritzing diagram** in their directory.    
For example, lbraries/MorePins/examples/Handle_One_Button/Sketch_one_button.fzz


**1. Include the component in your sketch or in main.cpp and use the namespace:**
```
#include <MoreInputs.h>
using LibMorePins;
```


	
**2. Instantiate the component:**
```
unsigned int numInputShiftRegisterICs = 1;  // How many ICs are used?
MoreInputs mi = MoreInputs(numInputShiftRegisterICs, MoreInputs::PINS_UNO_5_6_7);
// Using 1 IC on Arduino pins 5, 6, 7.
// 5 is for SH/LD
// 6 is for CLK
// 7 is for QH (Data)
```
 - Required parameters:
    - number of ICs that are used (unsigned int)
    - which pins are used (portMask)
 - (*) portMask is a structure defined in the library.
 
 - To use other pins, just change the second parameter when instantiating the it:
```
MoreInputs mi = MoreInputs(numInputShiftRegisterICs, MoreInputs::PINS_UNO_8_9_10);
```
With this code, you will use Arduino pins 8, 9 and 10.
The order is the following:

    - 1. SH/LD
    - 2. CLK
    - 3. QH (Data)
    


**3. Option 1: Assign a function to an icPin value change:**
```
mi.setHandlerForPinChange(icPinForFirstButton, handlerFirstButton);
```
Where:
  - *int icPinForFirstButton* is the icPinNumber (where is the button attached on the IC)
  - *void handlerFirstButton(uint8_t pinValue)* this is a function where you can do what you want with the pinValue when the button is pressed.

*Full working example:*
```
#include <Arduino.h>
#include <MoreInputs.h>

using LibMorePins;

// Instantiate MoreInputs in the "mi" object
MoreInputs mi = MoreInputs(numInputShiftRegisterICs, MoreInputs::PINS_UNO_8_9_10);

// Variable that holds the icPin numer for the button (where it is physically attached).
int icPinForFirstButton = 0;

// Declare the function that you will use for handling value changes for the button.
void handlerFirstButton(uint8_t icPinValue);

// Variable that stores the number of key presses on the button
int firstButtonPresses = 0;


void setup(){
	// Assign a handler function that will be called whenever the button is pressed.
	mi.setHandlerForPinChange(icPinForFirstButton, handlerFirstButton);
}

void loop(){
	// Call the loop method of the MoreInputs object.
	// Handler functions will not work without this
	mi.loop();
}

// Define the function that will be called when the button is pressed.
// Takes one argument, the pin value.
void handlerFirstButton(uint8_t icPinValue){
	// Determine if the change is triggered by the button being pressed.
	// Since it is a button with a pullup resistor, the value is LOW when the button
	// is pressed.
	if(icPinValue==LOW){
		// Button is pressed, do something with it.
		firstButtonPresses++;
	}
}
```


**4. Option 2: Just read a pin value whenever you want.**    
You can think of this exactly as you do with digitalRead.    
The method that does this is:    
```
uint8_t getPinValue(unsigned int icPinNumber);
```
**Example usage:**
```
// Variable to hold the pin number.
unsigned int icPinNumber = 5;

// Somewhere along the way, read the value of that pin.
uint8_t pinValue = mi.getPinValue(icPinNumber);

// Or do it in a conditional:
if(mi.getPinValue(icPinNumber) == LOW{
	// Do something is the value is low.
}
```

