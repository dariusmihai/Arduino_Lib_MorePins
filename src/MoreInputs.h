/*
 * MoreInputs.h
 *
 * @package: Library MorePins
 *
 *  Created on: Nov 27, 2017
 *  Author: Darius Mihai <dariusmihai@gmail.com>
 *
 *  Purpose: This part of the library provides a way to have many inputs on an Arduino Board.
 *  	This will allow the user to have as many inputs as they want, without
 *  	being limited by the number of pins available on a specific Arduino board.
 *
 *  This is achieved by using SN54HC165 or SN75HC165 shift registers.
 *
 *	All you need is 3 pins on the Arduino board:
 *		- SH/LD 	(OUTPUT)
 *		- CLK		(OUTPUT)
 *		- QH		(INPUT)
 *
 *	(!) ATTENTION: All 3 pins need to be on the same Arduino port.
 *
 *	For more info on ports, please consult:
 *	https://www.arduino.cc/en/Reference/PortManipulation
 *
 *  Each SN54HC165 has 8 inputs, so if you use:
 *  	- 1 x SN54HC165 = 8 inputs for 3 Arduino pins.
 *  	- 2 x SN54HC165 = 16 inputs for 3 Arduino pins.
 *  ... and so on
 *
 *
 *  	(*) INTERRUPTS NOT IMPLEMENTED YET (*)
 *  	The end purpose is to have these pins anywhere on the Arduino board.
 *  	However, depending on the board that is used, INTERRUPT can only be attached
 *  	to specific ports - see below table.
 *
 *  	More info about interrupts:
 *  	https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
 *
 *		Quick reference on interrupt pins (may be outdated, please check the above url:
 *
 *		=========================================================================
 *		| Board								| Interrupt Pins					|
 *		| ----------------------------------|-----------------------------------|
 *		| Uno, Nano, Mini, other 328-based	| 2, 3								|
 *		|-----------------------------------|-----------------------------------|
 *		| Mega, Mega2560, MegaADK			| 2, 3, 18, 19, 20, 21				|
 *		|-----------------------------------|-----------------------------------|
 *		| Micro, Leonardo, other 32u4-based	| 0, 1, 2, 3, 7						|
 *		|-----------------------------------|-----------------------------------|
 *		| Zero								|all digitals, except 4				|
 *		|-----------------------------------|-----------------------------------|
 *		| MKR1000 Rev.1						| 0, 1, 4, 5, 6, 7, 8, 9, A1, A2	|
 *		|-----------------------------------|-----------------------------------|
 *		| Due								| all digital pins					|
 *		|-----------------------------------|-----------------------------------|
 *		| 101								|	all digital pins 				|
 *		|									| (Only pins 2, 5, 7, 8, 10,		|
 *		|									| 11, 12, 13 work with CHANGE)		|
 *		=========================================================================
 *
 *
 *
 *
 */

#ifndef LIB_LIBMOREPINS_LIBMOREPINS_MOREINPUTS_H_
#define LIB_LIBMOREPINS_LIBMOREPINS_MOREINPUTS_H_

#include <Arduino.h>
#include <ArduinoSTL.h>

namespace LibMorePins{

	class MoreInputs {
	public:
		const uint8_t PINHIGH = 0x01;
		const uint8_t PINLOW = 0x00;

		struct portMask{
			char port;
			unsigned char mask;
		};

		// Always from MSB to LSB: IC QH (Data Read), IC CLK,  IC SH/LD
		static constexpr portMask PINS_UNO_4_5_6 = {port: 'D', mask: B01110000};
		static constexpr portMask PINS_UNO_5_6_7 = {port: 'D', mask: B11100000};
		static constexpr portMask PINS_UNO_8_9_10 = {port: 'B', mask: B00000111};
		static constexpr portMask PINS_UNO_9_10_11 = {port: 'B', mask: B00001110};
		static constexpr portMask PINS_UNO_10_11_12 = {port: 'B', mask: B00011100};
		static constexpr portMask PINS_UNO_11_12_13 = {port: 'B', mask: B00111000};

		/**
		 * Interrupts not implemented yet
		 */
		//LibMultiInputs_SN54HC165(unsigned int numRegisters, int interruptPin);

		/**
		 * Instantiate without interrupts
		 */
		MoreInputs(unsigned int numRegisters, portMask pins);


		/**
		 * A std::vector holding all the pins and their values.
		 */
		std::vector<uint8_t> pins;

		/**
		 * Returns the available number of pins.
		 */
		unsigned int getNumPins();

		/**
		 * Returns the value of the pin specified by pinNumber
		 */
		uint8_t getPinValue(unsigned int pinNumber);

		void readData();
		void readDataWithDebug(int pinFakeRead, int pinFakeClock);

		void debugAll();

		/**
		 * Sets the value for the amount of time in milliseconds
		 * that data can stay cached without triggering a new read
		 * on all the pins.
		 */
		void setDataCacheMicros(unsigned long dataCacheMicros);

		/**
		 * Sets the poll time in milliseconds used for
		 */
		void setPollTimeMillis(unsigned long pollTimeMillis);
		/**
		 * Operation mode 2.
		 * By calling this in your Arduino main loop,
		 * a complete read will be triggered every _pollTimeMillis milliseconds.
		 * When a change is detected in the values that are read,
		 * the function specified in setCompleteChangeHandler will be triggered.
		 *
		 * ATTENTION:
		 * This has to always be used in conjunction with either of the following:
		 * 	- setOverallChangeHandler
		 * 	- setPinChangeHandler
		 *
		 */
		void loop();
		void loopWithDebug(int pinFakeRead, int pinFakeClock);

		/**
		 * Sets a handler function for an overall change in the state of the inputs.
		 *
		 * ATTENTION:
		 * To be used together with loop();
		 * Call this object's loop function in the main Arduino loop(), then
		 * the handler that you previously have passed to setOverallChangeHandler
		 * in your setup() function will get called every time a change is detected.
		 *
		 * Example usage in your main Arduino sketch:
		 * void myHandlerFunc(std::vector<uint8_t> allPinsState){// Do something with allPinsState}
		 * void setup(){ MultiInputObj.setHandlerForOverallChange(myHandlerFunc); }
		 * void loop(){ MultiInputObj.loop(); }
		 *
		 */
		void setHandlerForOverallChange(void (*handlerFunc)(std::vector<uint8_t>));

		/**
		 * Sets a handler function for a change in the state of a specific pin.
		 *
		 * ATTENTION:
		 * To be used together with loop();
		 *
		 * Example usage in you main arduino sketch:
		 * void myButtonTwoHandlerFunc( uint8_t pinState ){ Do something depending on HIGH/LOW on IC Pin 2 }
		 * void setup(){ MultiInputObj.setHandlerForPinChange(2, myButtonTwoHandlerFunc); }
		 * void loop(){ MultiInputObj.loop() }
		 */
		void setHandlerForPinChange(unsigned int ICPinNumber, void(*handlerFunc)(uint8_t));

		virtual ~MoreInputs();

	protected:
		void _initEmptyHandlers();
		std::vector<void(*)(uint8_t)> _pinChangeHandlers;
		int pinSHLD = 0;
		int pinCLK = 0;
		int pinQH = 0;
		unsigned long _trackMicrosForCache = 0;
		unsigned long _dataCacheMicros = 100;
		void _initAllPins();
		void _findAllBits();
		unsigned char _bitSHLD = 0;
		unsigned char _bitCLK = 0;
		unsigned char _bitQH = 0;
		unsigned char _bitLowSHLD = 0;
		unsigned char _bitLowCLK = 0;
		unsigned char _bitLowQH = 0;
		unsigned int _numPinsPerIC = 8;
		char _port;
		unsigned char _pinMask;
		unsigned long _pollTimeTracker = 0;
		unsigned long _pollTimeMillis = 100;
		static void initInterrupt(int interruptPin, MoreInputs);
		unsigned int _numPins = 0;
		static void handleInterrupts();
		void _shLdHiLo(unsigned char pinState); // use ::PINHIGH and ::PINLOW here
		void _shLdLow(); // Sets the SH/LD pin low
		void _shLdHigh(); // Sets the SH/LD pin high
		void _clkHiLo(unsigned char pinState); // use ::PINHIGH and ::PINLOW here
		void _clkOnceLowHigh(); // Clocks the IC once, from Low to High
		void _clkOnceLowHighPortD();
		void _clkOnceLowHighPortB();
		void _clkLow(); // Sets the clock Low
		void _clkHigh(); // Sets the clock High
		/**
		 * Sets the inputs and outputs.
		 * SH/LD: output
		 * CLK: output
		 * QH: input
		 */
		void _setInputsOutputs();
		void _doNothing(uint8_t);
		void _doSomething(unsigned int pinNum);
	};

}
#endif /* LIB_LIBMOREPINS_LIBMOREPINS_MOREINPUTS_H_ */
