/*
 * MoreInputs.cpp
 *
 * @package: LibMorePins
 *
 *  Created on: Nov 27, 2017
 *      Author: Darius Mihai
 */

#include "MoreInputs.h"

using namespace LibMorePins;

MoreInputs::MoreInputs(unsigned int numRegisters, portMask pins)
{
	this->_numPins = numRegisters * 8;
	//this->pins.clear();
	//for(unsigned char i = 0; i < this->_numPins; i++)
	//{
	//	this->pins[i] = LOW;
	//}
	this->_initAllPins();
	this->_initEmptyHandlers();
	this->_port = pins.port;
	this->_pinMask = pins.mask;
	// Find all individual bits for the wires connected to the Arduino
	// This also calls _setInputsOutputs.
	this->_findAllBits();
	// Put SH/LD in Down state to enable parallel loading.
	this->_shLdHiLo(LOW);
	// Put the CLK in a low state.
	this->_clkHiLo(LOW);
}

void MoreInputs::_initEmptyHandlers()
{
	this->_pinChangeHandlers.clear();
	for(unsigned int i = 0; i < this->_numPins; i++)
	{
		this->_pinChangeHandlers.push_back(nullptr);
	}
}


void MoreInputs::_initAllPins()
{
	this->pins.clear();
	for(unsigned int i = 0; i < this->_numPins; i++)
	{
		this->pins.push_back(LOW);
	}
}

void MoreInputs::_findAllBits()
{
	// SH/LD and CLK need to be set as outouts
	// QH needs to be set as input
	// In MSB order, first and second bits are SH/LD and CLK,
	// the next being QH.
	unsigned char bitNumSHLD = 0;
	unsigned char bitNumCLK = 0;
	unsigned char bitNumQH = 0;
	unsigned char foundSoFar = 0;
	// counting from LSB to MSB,
	// find the positions for each of the 3 bits that we need.
	for (int i = 0; i<=7; i++)
	{
		if((this->_pinMask & B00000001 << i) == (B00000001 << i))
		{
			switch(foundSoFar)
			{
				case 0:
					bitNumSHLD = i;
					foundSoFar += 1;
					break;
				case 1:
					bitNumCLK = i;
					foundSoFar += 1;
					break;
				case 2:
					bitNumQH = i;
					foundSoFar += 1;
					break;
			}
		}

	}

	this->_bitSHLD = B00000001 << bitNumSHLD;
	this->_bitCLK = B00000001 << bitNumCLK;
	this->_bitQH = B00000001 << bitNumQH;

	this->_bitLowSHLD = ~(B00000001 << bitNumSHLD);
	this->_bitLowCLK = ~(B00000001 << bitNumCLK);
	this->_bitLowQH = ~(B00000001 << bitNumQH);

	if(this->_port == 'D')
	{
		this->pinSHLD = (int)bitNumSHLD;
		this->pinCLK = (int)bitNumCLK;
		this->pinQH = (int)bitNumQH;
	} else if(this->_port == 'B')
	{
		this->pinSHLD = 8 + (int)bitNumSHLD;
		this->pinCLK = 8 + (int)bitNumCLK;
		this->pinQH = 8 + (int)bitNumQH;
	}

	// Set inputs and outputs as needed.
	this->_setInputsOutputs();
}

unsigned int MoreInputs::getNumPins()
{
	return this->_numPins;
}

void MoreInputs::setDataCacheMicros( unsigned long dataCacheMicros )
{
	this->_dataCacheMicros = dataCacheMicros;
}

void MoreInputs::initInterrupt(int interruptPin, MoreInputs obj)
{
	attachInterrupt(digitalPinToInterrupt(interruptPin), obj.handleInterrupts, RISING);
}

void MoreInputs::handleInterrupts()
{
	Serial.begin(9600);
	Serial.println("Interrupted");
}

void MoreInputs::_setInputsOutputs()
{
	switch(this->_port)
	{
		case 'B':
			// Switch SH/LD and CLK HIGH (outputs)
			DDRB |= (this->_bitSHLD | this->_bitCLK);
			// Switch QH LOW (input)
			DDRB &= this->_bitLowQH;
			break;
		case 'D':
			// Switch SH/LD and CLK HIGH (outputs)
			DDRD |= (this->_bitSHLD | this->_bitCLK);
			// Switch QH LOW (input)
			DDRD &= this->_bitLowQH;
			break;
	}
}

void MoreInputs::_shLdHiLo(uint8_t pinState)
{
	//digitalWrite(this->pinSHLD, pinState);
	switch (this->_port)
	{
	case 'B':
		if(pinState == HIGH){
			PORTB = PINB | this->_bitSHLD;
		} else{
			PORTB = PINB & (this->_bitLowSHLD);
		}
		break;
	case 'D':
		if(pinState == HIGH){
			PORTD = PIND | this->_bitSHLD;
		}else{
			PORTD = PIND & (this->_bitLowSHLD);
		}
		break;
	}
}

void MoreInputs::_shLdLow()
{
	switch (this->_port)
	{
	case 'B':
		PORTB = PINB & (this->_bitLowSHLD);
		break;
	case 'D':
		PORTD = PIND & (this->_bitLowSHLD);
		break;
	}
}

void MoreInputs::_shLdHigh()
{
	switch (this->_port)
	{
	case 'B':
		PORTB = PINB | this->_bitSHLD;
		break;
	case 'D':
		PORTD = PIND | this->_bitSHLD;
		break;
	}
}
void MoreInputs::_clkHiLo(uint8_t pinState)
{
	//digitalWrite(this->pinCLK, pinState);

	switch(this->_port)
	{
	case 'D':
		if(pinState == HIGH){
			PORTD |= this->_bitCLK;
		}else{
			PORTD &= this->_bitLowCLK;
		}
		break;
	case 'B':
		if(pinState == HIGH){
			PORTB |= this->_bitCLK;
		} else{
			PORTB &= this->_bitLowCLK;
		}
		break;
	}
}

void MoreInputs::_clkOnceLowHigh()
{
	switch(this->_port)
	{
	case 'D':
		PORTD &= this->_bitLowCLK;
		PORTD |= this->_bitCLK;
		break;
	case 'B':
		PORTB &= this->_bitLowCLK;
		PORTB |= this->_bitCLK;
		break;
	}
}

void MoreInputs::_clkOnceLowHighPortD()
{
	PORTD &= this->_bitLowCLK;
	PORTD |= this->_bitCLK;
}

void MoreInputs::_clkOnceLowHighPortB()
{
	PORTB &= this->_bitLowCLK;
	PORTB |= this->_bitCLK;
}

void MoreInputs::_clkLow()
{
	switch(this->_port)
	{
	case 'D':
		PORTD &= this->_bitLowCLK;
		break;
	case 'B':
		PORTB &= this->_bitLowCLK;
		break;
	}
}

void MoreInputs::_clkHigh()
{
	switch(this->_port)
		{
		case 'D':
			PORTD |= this->_bitCLK;
			break;
		case 'B':
			PORTB |= this->_bitCLK;
			break;
		}
}

void MoreInputs::readData()
{
	// Start by putting SH/LD HIGH
	this->_clkLow();
	this->_shLdHigh();
	switch (this->_port)
	{
	case 'B':
		// Clock numPins times and read data on each low to high transition of the clock.
		for(unsigned int i = 0; i < this->_numPins; i++)
		{
			this->_clkOnceLowHighPortB();
			if((PINB & this->_bitQH) == this->_bitQH)
			{
				this->pins[i] = HIGH;
			} else {
				this->pins[i] = LOW;
			}
		}
		break;
	case 'D':
		for(unsigned int i = 0; i < this->_numPins; i++)
		{
			this->_clkOnceLowHighPortD();
			if((PIND & this->_bitQH) == this->_bitQH)
			{
				this->pins[i] = HIGH;
			} else {
				this->pins[i] = LOW;
			}
		}
		break;
	}
	this->_clkLow();
	this->_shLdLow();
}

void MoreInputs::readDataWithDebug(int pinFakeRead, int pinFakeClock)
{
	unsigned long start = micros();
	/////////////////////////
	// Actual readData code START
	this->readData();
	// Actual readData code END
	/////////////////////////
	unsigned long end = micros();
	uint8_t tmp[8];
	pinMode(pinFakeClock, OUTPUT);
	pinMode(pinFakeRead, INPUT);
	// The following for loop does the same thing as this->readData
	// but without actually affecting real data.
	// Clock once, read 1 bit, clock again, read next bit and so on.
	for(unsigned int i = 0; i < this->_numPins; i++){
		// Mimic a clock low to high
		digitalWrite(pinFakeClock, LOW);
		digitalWrite(pinFakeClock, HIGH);
		// Mimic a read
		tmp[i] = digitalRead(8);
	}

	unsigned long afterDigital = micros();
	unsigned long time = end - start;
	unsigned long digitalTime = afterDigital - end;
	Serial.println(String("In CLASS: Read 8 bits (microseconds): ") + time);
	Serial.println(String("In CLASS: DigitalRead & Write time: ") + digitalTime + String(" | Last Value: ") + tmp[7]);
	delay(2000);
}

unsigned char MoreInputs::getPinValue(unsigned int pinNumber)
{
	if(micros() - this->_trackMicrosForCache> this->_dataCacheMicros)
	{
		this->readData();
		this->_trackMicrosForCache = micros();
	}

	return this->pins[pinNumber];
	//return false;
}


void MoreInputs::setHandlerForPinChange(unsigned int icPinNum, void(*handlerFunc)(uint8_t))
{
	this->_pinChangeHandlers[icPinNum] = handlerFunc;
}

void MoreInputs::_doNothing(uint8_t val)
{

}

void MoreInputs::_doSomething(unsigned int pinNum)
{
	if(this->_pinChangeHandlers[pinNum] != nullptr)
	{
		(*(this->_pinChangeHandlers[pinNum]))(0);
	}
}

void MoreInputs::loop()
{
	if(millis() - this->_pollTimeTracker >= this->_pollTimeMillis)
	{
		std::vector<uint8_t> oldPins = this->pins;
		this->readData();
		for(unsigned int i = 0; i < this->_numPins; i++)
		{
			if(this->pins[i] != oldPins[i])
			{
				//if(this->_pinChangeHandlers[i] != nullptr)
				if((this->_pinChangeHandlers[i]) != nullptr)
				{
					(*(this->_pinChangeHandlers[i]))(this->pins[i]);
					//for(unsigned int j = 0; j<i; j++)
					//{
						//digitalWrite(12, HIGH);
						//delay(500);
						//this->_doSomething(i);
						//digitalWrite(12, LOW);
						//delay(500);
					//}

				} //else {
					//for(unsigned int j = 0; j<i; j++)
					//{
					//	digitalWrite(13, HIGH);
					//	delay(500);
					//	digitalWrite(13, LOW);
						//delay(500);
					//}
				//}
			}
		}
		this->_pollTimeTracker = millis();
	}
}

void MoreInputs::loopWithDebug(int pinFakeRead, int pinFakeClock)
{
	// Exact copy of loop, with only one change:
	// Instead of readData, call readDataWithDebug
	if(millis() - this->_pollTimeTracker >= this->_pollTimeMillis)
		{
			std::vector<uint8_t> oldPins = this->pins;
			this->readDataWithDebug(pinFakeRead, pinFakeClock);
			for(unsigned int i = 0; i < this->_numPins; i++)
			{
				if(this->pins[i] != oldPins[i])
				{
					//if(this->_pinChangeHandlers[i] != nullptr)
					if((this->_pinChangeHandlers[i]) != nullptr)
					{
						(*(this->_pinChangeHandlers[i]))(this->pins[i]);

					}
				}
			}
			this->_pollTimeTracker = millis();
		}
}

void MoreInputs::debugAll()
{
	Serial.begin(9600);
	Serial.println("Hello");
	Serial.println(String("Bit SHLD: ") + this->_bitSHLD);
	Serial.println(String("Bit CLK: ") + this->_bitCLK);
	Serial.println(String("Bit QH: ") + this->_bitQH);
	Serial.println(String("DDRD: ") + DDRD);
	Serial.println(String("PIND: ") + PIND);
	//Serial.println(String("SHLD Low: ") + this->_bitLowSHLD);
	//Serial.println(String("CLK Low: ") + this->_bitLowCLK);
	//Serial.println(String("QH Low: ") + this->_bitQH);

}

MoreInputs::~MoreInputs() {
	// TODO Auto-generated destructor stub
}



