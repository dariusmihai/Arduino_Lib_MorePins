#include <Arduino.h>
#include <MoreInputs.h>
using namespace LibMorePins;
/**
 * MorePins Library usage example
 * 
 * Scenario:
 * Run a function whenever a button is pressed.
 * 
 * This example will print a message on the serial monitor
 * each time the button is pressed.
 */


// Instantiate a MoreInputs object.
// We're using 1 SN54HC165 IC
// We're using Arduino pins 5, 6 and 7.
// 5 for SH/LD
// 6 for CLK
// 7 for QH (Data)
MoreInputs mi = MoreInputs(1, MoreInputs::PINS_UNO_5_6_7);

// Set the IC Pin number for our button.
unsigned int buttonIcPinNumber = 0;

// Create a variable that holds the number of times that the button is pressed.
int buttonWasPressedThisManyTimes = 0;

// Declare the function that we will use to handle button presses
void handlePressesForButton(uint8_t pinValue);

void setup() {
  // For debug:
  Serial.begin(9600);
  
  // put your setup code here, to run once:
  // Assign the handler function that we declared to the button.
  mi.setHandlerForPinChange(buttonIcPinNumber, handlePressesForButton);
}

void loop() {
  // put your main code here, to run repeatedly:
  // Call the loop() method of the MoreInputs object.
  mi.loop();
}

// Define the function that will handle the button presses.
void handlePressesForButton(uint8_t pinValue)
{
  // Using a pullup resistor on the button, so the 
  // pin is LOW when the button is pressed
  if(pinValue == LOW){
    buttonWasPressedThisManyTimes++;
    // We can even print something on serial 
    Serial.println(String("Button was pressed ") + buttonWasPressedThisManyTimes + String(" times"));
  }
}


