#include <Arduino.h>
#include <MoreInputs.h>

/**
 * Library usage example:
 * 6 Inputs: all buttons and Timing debug
 * Timing debug available through Serial.print
 */

using namespace LibMorePins;

unsigned int numInputShiftRegisterICs = 1;
MoreInputs mi = MoreInputs(numInputShiftRegisterICs, MoreInputs::PINS_UNO_5_6_7);

unsigned int icPinBtn0 = 0;
unsigned int icPinBtn1 = 1;
unsigned int icPinBtn2 = 2;
unsigned int icPinBtn3 = 3;
unsigned int icPinBtn4 = 4;
unsigned int icPinBtn5 = 5;

volatile unsigned int btn0Clicks = 0;
volatile unsigned int btn1Clicks = 0;
volatile unsigned int btn2Clicks = 0;
volatile unsigned int btn3Clicks = 0;
volatile unsigned int btn4Clicks = 0;
volatile unsigned int btn5Clicks = 0;

void handlerBtn0( uint8_t level );
void handlerBtn1( uint8_t level );
void handlerBtn2( uint8_t level );
void handlerBtn3( uint8_t level );
void handlerBtn4( uint8_t level );
void handlerBtn5( uint8_t level );

void debugFunc(unsigned long fullLoopTime);

unsigned long loopLargestTimeOver2Seconds;
unsigned long twoSecondTracker = 0;

void setup()
{
  Serial.begin(9600);
  mi.setHandlerForPinChange(icPinBtn0, handlerBtn0);
  mi.setHandlerForPinChange(icPinBtn1, handlerBtn1);
  mi.setHandlerForPinChange(icPinBtn2, handlerBtn2);
  mi.setHandlerForPinChange(icPinBtn3, handlerBtn3);
  mi.setHandlerForPinChange(icPinBtn4, handlerBtn4);
  mi.setHandlerForPinChange(icPinBtn5, handlerBtn5);
}

void loop()
{
  unsigned long start = micros();
  mi.loop();
  unsigned long end = micros();
  //mi.loopWithDebug(8,9);
  debugFunc(end-start);
}

void handlerBtn0( uint8_t level )
{
  if(level == LOW)
  {
    btn0Clicks++;
  }
}

void handlerBtn1( uint8_t level )
{
  if(level == LOW)
  {
    btn1Clicks++;
  }
}

void handlerBtn2( uint8_t level )
{
  if(level == LOW)
  {
    btn2Clicks++;
  }
}

void handlerBtn3( uint8_t level )
{
  if(level == LOW)
  {
    btn3Clicks++;
  }
}

void handlerBtn4( uint8_t level )
{
  if(level == LOW)
  {
    btn4Clicks++;
  }
}

void handlerBtn5( uint8_t level )
{
  if(level == LOW)
  {
    btn5Clicks++;
  }
}

void debugFunc(unsigned long fullLoopTime)
{
  if(fullLoopTime > loopLargestTimeOver2Seconds)
  {
    loopLargestTimeOver2Seconds = fullLoopTime;
  }
  if(millis() - twoSecondTracker >= 2000)
  {
    Serial.println("=====");
    Serial.println(String("Button 0: ") + btn0Clicks);
    Serial.println(String("Button 1: ") + btn1Clicks);
    Serial.println(String("Button 2: ") + btn2Clicks);
    Serial.println(String("Button 3: ") + btn3Clicks);
    Serial.println(String("Button 4: ") + btn4Clicks);
    Serial.println(String("Button 5: ") + btn5Clicks);
    Serial.println("-----");
    Serial.println(String("One full loop time (microseconds) including events: ") + fullLoopTime);
    Serial.println(String("Longest loop (micros) over 2 seconds: ") + loopLargestTimeOver2Seconds);
    Serial.println("=====");
    twoSecondTracker = millis();
    loopLargestTimeOver2Seconds = 0;
  }

}


